﻿using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsociacionDeportiva.BIZ
{
    public class ManejadorDeportes: IManejadorDeportes
    {
        IRepositorio<Deporte> repositorio;
        public ManejadorDeportes(IRepositorio<Deporte> repo)
        {
            repositorio = repo;
        }

        public List<Deporte> Listar => repositorio.Read.OrderBy(p => p.NombreDeporte).ToList();

        public bool Agregar(Deporte entidad)
        {
            return repositorio.Create(entidad);
        }


        public Deporte BuscarPorId(MongoDB.Bson.ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }



        public bool Eliminar(MongoDB.Bson.ObjectId id)
        {
            return repositorio.Delete(id);
        }


        public bool Modificar(Deporte entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}