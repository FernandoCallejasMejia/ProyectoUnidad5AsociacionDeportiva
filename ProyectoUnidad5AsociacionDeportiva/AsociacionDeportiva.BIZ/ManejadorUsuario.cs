﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AsociacionDeportiva.BIZ
{
    public class ManejadorUsuario
    {
        public string Archivo { get; set; }
        public ManejadorUsuario(string archivo)
        {
            Archivo = archivo;
        }
        public bool GuardarUsuario(string datos)
        {
            try
            {
                StreamWriter file = new StreamWriter(Archivo);
                file.Write(datos);
                file.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string LeerUsuario()
        {
            try
            {
                StreamReader file = new StreamReader(Archivo);
                string datos = file.ReadToEnd();
                file.Close();
                return datos;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}