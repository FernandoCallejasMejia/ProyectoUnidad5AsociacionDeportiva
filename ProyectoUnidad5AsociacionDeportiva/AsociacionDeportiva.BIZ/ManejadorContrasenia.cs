﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AsociacionDeportiva.BIZ
{
    public class ManejadorContrasenia
    {
        public string Archivo { get; set; }
        public ManejadorContrasenia(string archivo)
        {
            Archivo = archivo;
        }
        public bool GuardarContrasenia(string datos)
        {
            try
            {
                StreamWriter file = new StreamWriter(Archivo);
                file.Write(datos);
                file.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string LeerContrasenia()
        {
            try
            {
                StreamReader file = new StreamReader(Archivo);
                string datos = file.ReadToEnd();
                file.Close();
                return datos;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}