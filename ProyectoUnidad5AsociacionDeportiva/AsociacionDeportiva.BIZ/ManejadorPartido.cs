﻿using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsociacionDeportiva.BIZ
{
    public class ManejadorPartido : IManejadorPartido
    {
        IRepositorio<Partido> repositorio;
        public ManejadorPartido(IRepositorio<Partido> repo)
        {
            repositorio = repo;
        }

        public List<Partido> Listar => repositorio.Read;

        public bool Agregar(Partido entidad)
        {
            return repositorio.Create(entidad);
        }

        public Partido BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Partido entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
