﻿using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsociacionDeportiva.BIZ
{
    public class ManejadorTorneo : IManejadorTorneo
    {
        IRepositorio<Torneo> repositorio;
        public ManejadorTorneo(IRepositorio<Torneo> repo)
        {
            repositorio = repo;
        }
        public List<Torneo> Listar => repositorio.Read;

        public bool Agregar(Torneo entidad)
        {
            return repositorio.Create(entidad);
        }

        public Torneo BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault(); ;
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Torneo entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
