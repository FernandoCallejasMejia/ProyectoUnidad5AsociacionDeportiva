﻿using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsociacionDeportiva.BIZ
{
    public class ManejadorEquipos : IManejadorEquipos
    {
        IRepositorio<Equipo> repositorio;
        public ManejadorEquipos(IRepositorio<Equipo> repo)
        {
            repositorio = repo;
        }

        public List<Equipo> Listar => repositorio.Read.OrderBy(p => p.NombreEquipo).ToList();

        public bool Agregar(Equipo entidad)
        {
            return repositorio.Create(entidad);
        }

        public Equipo BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorio.Delete(id);
        }

        public List<Equipo> EquiposDeporte(string deporte)
        {
            return Listar.Where(e => e.Deporte == deporte).ToList();
        }

        public bool Modificar(Equipo entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}