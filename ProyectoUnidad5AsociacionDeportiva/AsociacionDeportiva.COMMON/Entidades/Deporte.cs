﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsociacionDeportiva.COMMON.Entidades
{
    public class Deporte:Base
    {
        public string NombreDeporte { get; set; }
        public override string ToString()
        {
            return string.Format("{0}",NombreDeporte );
        }
    }
}
