﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsociacionDeportiva.COMMON.Entidades
{
    public class Torneo: Base
    {
        public Deporte deporte { get; set; }
        public List<Equipo> EquiposParticipantes { get; set; }
    }
}
