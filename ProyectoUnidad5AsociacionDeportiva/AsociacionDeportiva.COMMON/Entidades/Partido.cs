﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsociacionDeportiva.COMMON.Entidades
{
    public class Partido:Base
    {
        public Equipo EquipoLocal { get; set; }
        public Equipo EquipoVisitante { get; set; }
        public int MarcadorLocal { get; set; }
        public int MarcadorVisitante { get; set; }
    }
}
