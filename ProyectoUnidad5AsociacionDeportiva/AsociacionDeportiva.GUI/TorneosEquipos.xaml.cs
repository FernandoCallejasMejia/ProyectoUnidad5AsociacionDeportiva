﻿using AsociacionDeportiva.BIZ;
using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AsociacionDeportiva.GUI
{
    /// <summary>
    /// Lógica de interacción para TorneosEquipos.xaml
    /// </summary>
    public partial class TorneosEquipos : Window
    {
        enum Accion
        {
            Nuevo,
            Editar
        }
        ManejadorTorneo manejadorDeTorneos;
        ManejadorEquipos manejadorEquipos;
        ManejadorDeportes manejadorDeportes;
        //ManejadorPartido manejadorPartidos;
        Torneo torneo;
        //List<Equipo> equipos;
        //List<int> randoms;

        Accion accionTorneo;
        public TorneosEquipos()
        {
            InitializeComponent();
            //randoms = new List<int>();
            manejadorDeTorneos = new ManejadorTorneo(new RepositorioGenerico<Torneo>());
            manejadorEquipos = new ManejadorEquipos(new RepositorioGenerico<Equipo>());
            manejadorDeportes = new ManejadorDeportes(new RepositorioGenerico<Deporte>());
            //manejadorPartidos = new ManejadorPartido(new RepositorioGenerico<Partido>());
            //equipos = new List<Equipo>();

            ActualizarTablaDeTorneos();
            ActualizarCombosDeporte();
            ActualizarCombosEquipo();
            gridTorneo.IsEnabled = false;
        }

        private void ActualizarTablaDeTorneos()
        {
            dtgTorneos.ItemsSource = null;
            dtgTorneos.ItemsSource = manejadorDeTorneos.Listar;
        }

        private void ActualizarCombosDeporte()
        {
            cmbDeporte.ItemsSource = null;
            cmbDeporte.ItemsSource = manejadorDeportes.Listar;
        }
        private void ActualizarCombosEquipo()
        {
            cmbEquipos.ItemsSource = null;
            cmbEquipos.ItemsSource = manejadorEquipos.Listar;
        }

        private void ActualizarListaDeEquiposEnTorneo()
        {
            dtgEquiposEnTorneo.ItemsSource = null;
            dtgEquiposEnTorneo.ItemsSource = torneo.EquiposParticipantes;
        }

        private void LimpiarCamposDeTorneo()
        {
            dtgEquiposEnTorneo.ItemsSource = null;
            cmbEquipos.Text = null;
            cmbDeporte.Text = null;

        }
        //private void btnGenerar_Click(object sender, RoutedEventArgs e)
        //{

        //}
        //private void CrearTorneo()
        //{
        //    Random r = new Random();
        //    Random ran = new Random();

        //    if (!GenerarPartido(r.Next(0, equipos.Count - 1), ran.Next(0, equipos.Count - 1)))
        //    {
        //        CrearTorneo();
        //    }
        //}
        //private bool GenerarPartido(int uno, int dos)
        //{
        //    if (randoms.Contains(uno) || randoms.Contains(dos))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        manejadorPartidos.Agregar(new Partido()
        //        {
        //            EquipoLocal = equipos[uno],
        //            EquipoVisitante = equipos[dos]
        //        });
        //        randoms.Add(uno);
        //        randoms.Add(dos);
        //        return true;
        //    }
        //}

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionTorneo == Accion.Nuevo)
            {
                torneo.deporte = cmbDeporte.SelectedItem as Deporte;
                if (string.IsNullOrEmpty(cmbDeporte.Text) || string.IsNullOrEmpty(cmbEquipos.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorDeTorneos.Agregar(torneo))
                    {
                        MessageBox.Show("Torneo guardado con exito", "Torneo", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        LimpiarCamposDeTorneo();
                        gridTorneo.IsEnabled = false;
                        ActualizarTablaDeTorneos();
                        //for (int i = 0; i < equipos.Count / 2; i++)
                        //{
                        //    CrearTorneo();

                        //}
                    }
                    else
                    {
                        MessageBox.Show("Error al guardar el torneo", "Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                torneo.deporte = cmbDeporte.SelectedItem as Deporte;
                if (manejadorDeTorneos.Modificar(torneo))
                {
                    MessageBox.Show("Torneo guardado con éxito", "Torneo", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    LimpiarCamposDeTorneo();
                    gridTorneo.IsEnabled = false;
                    ActualizarTablaDeTorneos();
                }
                else
                {
                    MessageBox.Show("Error al guardar el Torneo", "Torneo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeTorneo();
            gridTorneo.IsEnabled = false;
        }

        private void btnEliminarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipo eq = dtgEquiposEnTorneo.SelectedItem as Equipo;
            if (eq != null)
            {
                torneo.EquiposParticipantes.Remove(eq);
                ActualizarListaDeEquiposEnTorneo();
            }
        }

        private void btnAgregarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipo eq = cmbEquipos.SelectedItem as Equipo;
            if (eq != null)
            {
                torneo.EquiposParticipantes.Add(eq);
                ActualizarListaDeEquiposEnTorneo();
                //equipos.Add(eq);
            }
        }

        private void btnNuevoTorneo_Click(object sender, RoutedEventArgs e)
        {
            gridTorneo.IsEnabled = true;
            ActualizarCombosDeporte();
            ActualizarCombosEquipo();
            torneo = new Torneo();
            torneo.EquiposParticipantes = new List<Equipo>();
            ActualizarListaDeEquiposEnTorneo();
            accionTorneo = Accion.Nuevo;
        }

        private void btnEliminarTorneo_Click(object sender, RoutedEventArgs e)
        {
            Torneo t = dtgTorneos.SelectedItem as Torneo;
            if (t != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar el Torneo?", "Torneos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeTorneos.Eliminar(t.Id))
                    {
                        MessageBox.Show("Eliminado con éxito", "Torneos", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaDeTorneos();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Torneos", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void dtgTorneos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Torneo t = dtgTorneos.SelectedItem as Torneo;
            if (t != null)
            {
                gridTorneo.IsEnabled = true;
                torneo = t;
                ActualizarListaDeEquiposEnTorneo();
                accionTorneo = Accion.Editar;
                ActualizarCombosEquipo();
                ActualizarCombosDeporte();
                cmbDeporte.Text = torneo.deporte.ToString();
                cmbEquipos.Text = torneo.EquiposParticipantes.ToString();
            }
        }
    }
}
