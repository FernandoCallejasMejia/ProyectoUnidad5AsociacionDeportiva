﻿#pragma checksum "..\..\Torneo.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "EACE0560E7926C9802EB57B716C949D9A12601D5"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using AsociacionDeportiva.GUI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AsociacionDeportiva.GUI {
    
    
    /// <summary>
    /// Torneo
    /// </summary>
    public partial class Torneo : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoTorneo;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminarTorneo;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgTorneos;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridTorneo;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbDeporte;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbEquipos;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAgregarEquipo;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminarEquipo;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgMaterialesEnVale;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGenerar;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardar;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\Torneo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCanelar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AsociacionDeportiva.GUI;component/torneo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Torneo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnNuevoTorneo = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\Torneo.xaml"
            this.btnNuevoTorneo.Click += new System.Windows.RoutedEventHandler(this.btnNuevoTorneo_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnEliminarTorneo = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\Torneo.xaml"
            this.btnEliminarTorneo.Click += new System.Windows.RoutedEventHandler(this.btnEliminarTorneo_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.dtgTorneos = ((System.Windows.Controls.DataGrid)(target));
            
            #line 25 "..\..\Torneo.xaml"
            this.dtgTorneos.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.dtgVales_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 4:
            this.gridTorneo = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.cmbDeporte = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.cmbEquipos = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.btnAgregarEquipo = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\Torneo.xaml"
            this.btnAgregarEquipo.Click += new System.Windows.RoutedEventHandler(this.btnAgregarEquipo_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnEliminarEquipo = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\Torneo.xaml"
            this.btnEliminarEquipo.Click += new System.Windows.RoutedEventHandler(this.btnEliminarEquipo_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.dtgMaterialesEnVale = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 10:
            this.btnGenerar = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\Torneo.xaml"
            this.btnGenerar.Click += new System.Windows.RoutedEventHandler(this.btnGenerar_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnGuardar = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\Torneo.xaml"
            this.btnGuardar.Click += new System.Windows.RoutedEventHandler(this.btnGuardar_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnCanelar = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\Torneo.xaml"
            this.btnCanelar.Click += new System.Windows.RoutedEventHandler(this.btnCanelar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

