﻿using AsociacionDeportiva.BIZ;
using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.COMMON.Interfaces;
using AsociacionDeportiva.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AsociacionDeportiva.GUI
{
    /// <summary>
    /// Lógica de interacción para Deportes.xaml
    /// </summary>
    public partial class Deportes : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorDeportes manejadorDeportes;
        accion accionDeportes;
        public Deportes()
        {
            InitializeComponent();
            manejadorDeportes = new ManejadorDeportes(new RepositorioGenerico<Deporte>());

            PonerBotonesDeportesEnEdicion(false);
            LimpiarCamposDeDeportes();
            ActualizarTablaDeportes();
        }
        private void ActualizarTablaDeportes()
        {
            dtgDeportes.ItemsSource = null;
            dtgDeportes.ItemsSource = manejadorDeportes.Listar;
        }

        private void LimpiarCamposDeDeportes()
        {
            txbNombreDeporte.Clear();
        }

        private void PonerBotonesDeportesEnEdicion(bool value)
        {
            btnNuevoDeporte.IsEnabled = !value;
            btnCancelarDeporte.IsEnabled = value;
            btnEditarDeporte.IsEnabled = !value;
            btnEliminarDeporte.IsEnabled = !value;
            btnGuardarDeporte.IsEnabled = value;
        }

        private void btnEliminarDeporte_Click(object sender, RoutedEventArgs e)
        {
            Deporte d = dtgDeportes.SelectedItem as Deporte;
            if (d != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este Deporte?", "Deportes", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeportes.Eliminar(d.Id))
                    {
                        MessageBox.Show("Deporte eliminado", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeportes();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Deporte", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCancelarDeporte_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeDeportes();
            PonerBotonesDeportesEnEdicion(false);
        }

        private void btnGuardarDeporte_Click(object sender, RoutedEventArgs e)
        {
            if (accionDeportes == accion.Nuevo)
            {
                Deporte d = new Deporte()
                {
                    NombreDeporte = txbNombreDeporte.Text,
                };
                if (string.IsNullOrEmpty(txbNombreDeporte.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorDeportes.Agregar(d))
                    {
                        MessageBox.Show("Deporte agregado correctamente", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeDeportes();
                        ActualizarTablaDeportes();
                        PonerBotonesDeportesEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Deporte no se pudo agregar", "Deportes", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Deporte dep = dtgDeportes.SelectedItem as Deporte;
                dep.NombreDeporte = txbNombreDeporte.Text;
                if (manejadorDeportes.Modificar(dep))
                {
                    MessageBox.Show("Deporte modificado correctamente", "Deportes", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeDeportes();
                    ActualizarTablaDeportes();
                    PonerBotonesDeportesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Deporte no se pudo actualizar", "Deportes", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEditarDeporte_Click(object sender, RoutedEventArgs e)
        {
            Deporte d = dtgDeportes.SelectedItem as Deporte;
            if (d != null)
            {
                txbDeportesId.Text = d.Id.ToString();
                txbNombreDeporte.Text = d.NombreDeporte;
                accionDeportes = accion.Editar;
                PonerBotonesDeportesEnEdicion(true);
            }
        }

        private void btnNuevoDeporte_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeDeportes();
            PonerBotonesDeportesEnEdicion(true);
            accionDeportes = accion.Nuevo;
        }
    }
}