﻿using AsociacionDeportiva.BIZ;
using AsociacionDeportiva.COMMON.Entidades;
using AsociacionDeportiva.COMMON.Interfaces;
using AsociacionDeportiva.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AsociacionDeportiva.GUI
{
    /// <summary>
    /// Lógica de interacción para Equipos.xaml
    /// </summary>
    public partial class Equipos : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorEquipos manejadorEquipos;
        IManejadorDeportes manejadorDeportes;
        accion accionEquipos;
        public Equipos()
        {
            InitializeComponent();
            manejadorEquipos = new ManejadorEquipos(new RepositorioGenerico<Equipo>());
            manejadorDeportes = new ManejadorDeportes(new RepositorioGenerico<Deporte>());

            PonerBotonesEquiposEnEdicion(false);
            LimpiarCamposDeEquipos();
            ActualizarTablaEquipos();
            ActualizarCombosDeporte();
        }

        private void ActualizarTablaEquipos()
        {
            dtgEquipos.ItemsSource = null;
            dtgEquipos.ItemsSource = manejadorEquipos.Listar;
        }

        private void LimpiarCamposDeEquipos()
        {
            txbNombreEquipo.Clear();
        }

        private void PonerBotonesEquiposEnEdicion(bool value)
        {
            btnNuevoEquipo.IsEnabled = !value;
            btnCancelarEquipo.IsEnabled = value;
            btnEditarEquipo.IsEnabled = !value;
            btnEliminarEquipo.IsEnabled = !value;
            btnGuardarEquipo.IsEnabled = value;
        }
        private void btnCancelarEquipo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEquipos();
            PonerBotonesEquiposEnEdicion(false);
        }

        private void ActualizarCombosDeporte()
        {
            cmbDeporteEquipo.ItemsSource = null;
            cmbDeporteEquipo.ItemsSource = manejadorDeportes.Listar;
        }
        private void btnEliminarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipo eq = dtgEquipos.SelectedItem as Equipo;
            if (eq != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este Equipo?", "Equipos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEquipos.Eliminar(eq.Id))
                    {
                        MessageBox.Show("Equipo eliminado", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEquipos();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Equipo", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnGuardarEquipo_Click(object sender, RoutedEventArgs e)
        {
            if (accionEquipos == accion.Nuevo)
            {
                Equipo eq = new Equipo()
                {
                    NombreEquipo = txbNombreEquipo.Text,
                    Deporte = cmbDeporteEquipo.Text
                };
                if (string.IsNullOrEmpty(txbNombreEquipo.Text) || string.IsNullOrEmpty(cmbDeporteEquipo.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorEquipos.Agregar(eq))
                    {
                        MessageBox.Show("Equipo agregado correctamente", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeEquipos();
                        ActualizarTablaEquipos();
                        PonerBotonesEquiposEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Equipo no se pudo agregar", "Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Equipo equ = dtgEquipos.SelectedItem as Equipo;
                equ.NombreEquipo = txbNombreEquipo.Text;
                equ.Deporte = cmbDeporteEquipo.Text;
                if (manejadorEquipos.Modificar(equ))
                {
                    MessageBox.Show("Equipo modificado correctamente", "Equipos", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEquipos();
                    ActualizarTablaEquipos();
                    PonerBotonesEquiposEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Equipo no se pudo modificar", "Equipos", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEditarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipo eq = dtgEquipos.SelectedItem as Equipo;
            if (eq != null)
            {
                ActualizarCombosDeporte();
                txbEquiposId.Text = eq.Id.ToString();
                txbNombreEquipo.Text = eq.NombreEquipo;
                cmbDeporteEquipo.Text = eq.Deporte;
                accionEquipos = accion.Editar;
                PonerBotonesEquiposEnEdicion(true);
            }
        }

        private void btnNuevoEquipo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEquipos();
            PonerBotonesEquiposEnEdicion(true);
            ActualizarCombosDeporte();
            accionEquipos = accion.Nuevo;
        }
    }
}
