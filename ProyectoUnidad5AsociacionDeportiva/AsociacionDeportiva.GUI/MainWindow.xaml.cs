﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsociacionDeportiva.GUI
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string contrasenia="administrador";
        string Usuario = "Administrador";
        public MainWindow()
        {
            InitializeComponent();
            Datos datos = new Datos();
        }

        private void btnEntrar_Click(object sender, RoutedEventArgs e)
        {
            Datos datos = new Datos();
            if (string.IsNullOrEmpty(txbContrasenia.Text) || string.IsNullOrEmpty(txbUsuario.Text))
            {
                MessageBox.Show("Faltan Usuario o Contraseña", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                if (txbContrasenia.Text == "administrador" && txbUsuario.Text == "Administrador")
                {
                    MessageBox.Show("Acceso Correcto", "Bienvenido");
                    datos.Show();
                }
                else
                {
                    MessageBox.Show("Contraseña o Usuario incorrecto", "Error");
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            txbContrasenia.Text = "";
            txbUsuario.Text = "";
        }
    }
}
