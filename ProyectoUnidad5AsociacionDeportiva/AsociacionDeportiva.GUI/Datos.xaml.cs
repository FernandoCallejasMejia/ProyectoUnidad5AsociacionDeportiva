﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AsociacionDeportiva.GUI
{
    /// <summary>
    /// Lógica de interacción para Datos.xaml
    /// </summary>
    public partial class Datos : Window
    {
        public Datos()
        {
            InitializeComponent();
        }

        private void btnAgregarDeporte_Click(object sender, RoutedEventArgs e)
        {
            Deportes deportes = new Deportes();
            deportes.Show();
        }

        private void btnAgregarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipos equipos = new Equipos();
            equipos.Show();
        }

        private void btnTorneo_Click(object sender, RoutedEventArgs e)
        {
            TorneosEquipos torneosEquipos = new TorneosEquipos();
            torneosEquipos.Show();
        }
    }
}
